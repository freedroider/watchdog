package bm.poc.com.watchdogapp.tracking

import android.app.Notification
import android.app.NotificationManager
import android.app.Service
import android.app.usage.UsageEvents
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.text.TextUtils
import android.util.Log
import bm.poc.com.watchdogapp.R
import bm.poc.com.watchdogapp.core.Prefs
import java.util.*


class TrackingService : Service() {
    val timerExecutor = Timer()
    lateinit var usageStatsManager: UsageStatsManager
    lateinit var notificationManager: NotificationManager

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onCreate() {
        super.onCreate()
        usageStatsManager = getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (!Prefs.isStopped(this)) {
            tracking()
            return START_STICKY
        }
        return START_NOT_STICKY
    }

    private fun tracking() {
        val delay = Prefs.getDelay(this)
        val timeTracking = Prefs.getTimeMs(this)
        val trackedPackageName = Prefs.getTrackedPackageName(this)
        timerExecutor.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                if (isRunning(delay, trackedPackageName)) {
                    val trackedTime = System.currentTimeMillis() - Prefs.getTrackedTimeMs(applicationContext)
                    if (trackedTime >= timeTracking) {
                        Prefs.saveStopped(this@TrackingService, true)
                        showNotification()
                        cancel()
                        stopSelf()
                    }
                }
            }
        }, 0L, delay)
    }

    fun isRunning(delay: Long, packageName: String?): Boolean {
        val time = System.currentTimeMillis()
        val start = time - (delay * 2)
        val appList = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, start, time)
        appList?.filter {
            TextUtils.equals(it.packageName, packageName)
        }?.forEach {
            val field = it::class.java.getDeclaredField("mLastEvent")
            field.isAccessible = true
            val lastState = field.get(it) as Int
            if (lastState == UsageEvents.Event.MOVE_TO_FOREGROUND) {
                Log.d("TrackingService", "foreground")
                Prefs.saveIsAppRunning(applicationContext, true)
                Prefs.saveTrackedTimeMs(applicationContext, it.lastTimeUsed)
            } else {
                Log.d("TrackingService", "not foreground")
                Prefs.saveIsAppRunning(applicationContext, false)
                Prefs.saveTrackedTimeMs(applicationContext, 0L)
            }
        }
        return Prefs.isAppRunning(applicationContext)
    }

    private fun showNotification() {
        val notification = NotificationCompat.Builder(this).apply {
            setSmallIcon(R.mipmap.ic_launcher)
            setContentTitle("Event triggered")
            setContentText("Bum bum bum")
            priority = Notification.PRIORITY_MAX
            setDefaults(Notification.DEFAULT_SOUND with Notification.DEFAULT_VIBRATE)
        }.build()
        notificationManager.notify(102, notification)
    }

    infix fun Int.with(x: Int) = this.or(x)
}