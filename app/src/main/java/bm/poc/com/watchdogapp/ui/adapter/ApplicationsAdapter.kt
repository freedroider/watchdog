package bm.poc.com.watchdogapp.ui.adapter

import android.content.pm.ApplicationInfo
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bm.poc.com.watchdogapp.R
import kotlinx.android.synthetic.main.item_application.view.*

class ApplicationsAdapter(val items: List<ApplicationInfo>) : RecyclerView.Adapter<ApplicationsAdapter.ViewHolder>(), OnItemClickListener {
    var itemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder
            = ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_application, parent, false), this)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.fill(items[position])
    }

    override fun onItemClick(position: Int, view: View) {
        itemClickListener?.onItemClick(position, view)
    }

    class ViewHolder(itemView: View, itemClickListener: OnItemClickListener) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                itemClickListener.onItemClick(adapterPosition, itemView)
            }
        }

        fun fill(item: ApplicationInfo) {
            itemView.ivAppIcon.setImageDrawable(item.loadIcon(itemView.context.packageManager))
            itemView.tvAppLabel.text = item.loadLabel(itemView.context.packageManager)
            itemView.tvAppPackageName.text = item.packageName
        }
    }
}

interface OnItemClickListener {
    fun onItemClick(position: Int, view: View)
}