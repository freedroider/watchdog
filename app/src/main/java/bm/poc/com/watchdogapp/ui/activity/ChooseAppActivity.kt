package bm.poc.com.watchdogapp.ui.activity

import android.app.Activity
import android.app.AppOpsManager
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import bm.poc.com.watchdogapp.R
import bm.poc.com.watchdogapp.ui.adapter.ApplicationsAdapter
import bm.poc.com.watchdogapp.ui.adapter.OnItemClickListener
import kotlinx.android.synthetic.main.activity_choose_app.*


const val APPLICATION_INFO_BUNDLE_KEY = "ApplicationInfoBundle"

class ChooseAppActivity : AppCompatActivity(), OnItemClickListener {
    companion object {

        fun start(activityContext: Activity, requestCode: Int) {
            val startIntent = Intent(activityContext, ChooseAppActivity::class.java)
            activityContext.startActivityForResult(startIntent, requestCode)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_app)
        setTitle(R.string.choose_an_application)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val installedApplications = packageManager.getInstalledApplications(PackageManager.GET_META_DATA).filter {
            (it.flags and ApplicationInfo.FLAG_SYSTEM) == 0
        }
        val adapter = ApplicationsAdapter(installedApplications)
        rvApps.layoutManager = LinearLayoutManager(this)
        rvApps.setHasFixedSize(true)
        rvApps.adapter = adapter
        adapter.itemClickListener = this
    }

    override fun onResume() {
        super.onResume()
        checkPermission()
    }

    override fun onItemClick(position: Int, view: View) {
        val adapter = rvApps.adapter as ApplicationsAdapter
        val returnedIntent = Intent()
        val bundle = Bundle(1)
        bundle.putParcelable(APPLICATION_INFO_BUNDLE_KEY, adapter.items[position])
        returnedIntent.putExtras(bundle)
        setResult(RESULT_OK, returnedIntent)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(RESULT_CANCELED)
    }

    private fun checkPermission() {
        val granted : Boolean
        val appOps = getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
        val mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, android.os.Process.myUid(), packageName)
        if (mode == AppOpsManager.MODE_DEFAULT) {
            val per = checkCallingOrSelfPermission(android.Manifest.permission.PACKAGE_USAGE_STATS)
            granted = per == PackageManager.PERMISSION_GRANTED
        } else {
            granted = mode == AppOpsManager.MODE_ALLOWED
        }
        if (!granted) {
            Toast.makeText(this, "Please turn on permission", Toast.LENGTH_SHORT).show()
            startActivity(Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS))
        }
    }
}