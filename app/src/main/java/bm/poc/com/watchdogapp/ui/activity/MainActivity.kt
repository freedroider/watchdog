package bm.poc.com.watchdogapp.ui.activity

import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import bm.poc.com.watchdogapp.R
import bm.poc.com.watchdogapp.core.Prefs
import bm.poc.com.watchdogapp.tracking.TrackingService
import bm.poc.com.watchdogapp.ui.dialog.SetTimeoutDialogFragment
import kotlinx.android.synthetic.main.activity_main.*

const val CHOOSE_APP_REQUEST_CODE = 189

class MainActivity : AppCompatActivity(), OnTrackedTimeListener {
    override fun onOk() {
        fillTimeOut()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CHOOSE_APP_REQUEST_CODE -> {
                val applicationInfo = data?.extras?.get(APPLICATION_INFO_BUNDLE_KEY) as? ApplicationInfo
                if (applicationInfo != null) {
                    fillAppInfo(applicationInfo)
                }
            }
        }
    }

    private fun fillAppInfo(applicationInfo: ApplicationInfo) {
        Prefs.saveTrackedPackageName(this, applicationInfo.packageName)
        tvAppText.text = applicationInfo.loadLabel(packageManager)
        ivAppIcon.setImageDrawable(applicationInfo.loadIcon(packageManager))
    }

    private fun fillTimeOut() {
        val time = Prefs.getTime(this)
        if (time == 0L) return
        val type = Prefs.getType(this)
        tvTimeout.text = time.toString()
        tvTimeoutSecondary.text = Prefs.toTimeType(type)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        llChooseApplication.setOnClickListener {
            ChooseAppActivity.start(this, CHOOSE_APP_REQUEST_CODE)
        }
        llSetTimeout.setOnClickListener {
            SetTimeoutDialogFragment.newInstance().show(supportFragmentManager, "TAG")
        }
        btnStartTracking.setOnClickListener {
            if (isValidInput()) {
                Prefs.saveStopped(this, false)
                Prefs.saveTrackedTimeMs(this, 0L)
                Prefs.saveIsAppRunning(this, false)
                val startServiceIntent = Intent(this, TrackingService::class.java)
                startService(startServiceIntent)
                Toast.makeText(this, "The tracking service is started", Toast.LENGTH_SHORT).show()
                finish()
            } else{
                Toast.makeText(this, "Please choose an application and set the tracking time", Toast.LENGTH_SHORT).show()

            }
        }
        val trackedPackageName = Prefs.getTrackedPackageName(this)
        if (trackedPackageName != null) {
            val installedApplications = packageManager.getInstalledApplications(PackageManager.GET_META_DATA).filter {
                TextUtils.equals(trackedPackageName, it.packageName)
            }
            if (!installedApplications.isEmpty()) {
                val applicationInfo = installedApplications[0]
                if (applicationInfo != null) {
                    fillAppInfo(applicationInfo)
                    fillTimeOut()
                }
            }
        }
    }

    private fun isValidInput(): Boolean = !TextUtils.isEmpty(Prefs.getTrackedPackageName(this)) and (Prefs.getTime(this) != 0L)
}

interface OnTrackedTimeListener {
    fun onOk()
}