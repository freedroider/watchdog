package bm.poc.com.watchdogapp.core

import android.content.Context
import android.content.SharedPreferences
import android.support.annotation.IntDef


object Prefs {
    const val PREFS_NAME = "appPreference.preference"
    const val TIME_TYPE = "timeType"
    const val TIME = "time"
    const val DELAY = "delay"
    const val TRACKED_TIME = "trackedTime"
    const val TRACKED_PACKEAGE_NAME = "trackedPackageName"
    const val STOPPED = "stopped"
    const val IS_APP_RUNNING = "lastAppState"

    const val DEFAULT_DELAY = 5 * 1000L // 5 seconds

    @IntDef(SECONDS, MINUTES, HOURS)
    @Retention(AnnotationRetention.SOURCE)
    annotation class TimeType

    const val SECONDS = 1L
    const val MINUTES = 2L
    const val HOURS = 3L

    @TimeType
    fun toTimeType(value: String): Long {
        when (value) {
            "Seconds" -> {
                return SECONDS
            }
            "Minutes" -> {
                return MINUTES
            }
            "Hours" -> {
                return HOURS
            }
            else -> {
                throw IllegalArgumentException()
            }
        }
    }

    fun toTimeType(value: Long): String {
        when (value) {
            SECONDS -> {
                return "Seconds"
            }
            MINUTES -> {
                return "Minutes"
            }
            HOURS -> {
                return "Hours"
            }
            else -> {
                throw IllegalArgumentException()
            }
        }
    }

    private fun getSharedPreference(context: Context): SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun saveType(context: Context, type: String) {
        getSharedPreference(context).edit()
                .putLong(TIME_TYPE, toTimeType(type))
                .apply()
    }

    fun getType(context: Context): Long = getSharedPreference(context).getLong(TIME_TYPE, SECONDS)

    fun saveTime(context: Context, time: Long) {
        getSharedPreference(context).edit().putLong(TIME, time).apply()
    }

    fun getTime(context: Context): Long {
        return getSharedPreference(context).getLong(TIME, 0L)
    }

    fun getTimeMs(context: Context): Long {
        val timeType = getType(context)
        val k: Long
        when (timeType) {
            SECONDS -> {
                k = 1000
            }
            MINUTES -> {
                k = 1000 * 60
            }
            HOURS -> {
                k = 1000 * 60 * 60
            }
            else -> {
                throw IllegalArgumentException()
            }
        }
        return getTime(context) * k
    }

    fun saveDelay(context: Context, delay: Long) {
        getSharedPreference(context).edit().putLong(DELAY, delay).apply()
    }

    fun getDelay(context: Context): Long = getSharedPreference(context).getLong(DELAY, DEFAULT_DELAY)

    fun saveTrackedTimeMs(context: Context, time: Long) {
        getSharedPreference(context).edit().putLong(TRACKED_TIME, time).apply()
    }

    fun getTrackedTimeMs(context: Context): Long = getSharedPreference(context).getLong(TRACKED_TIME, 0L)

    fun saveTrackedPackageName(context: Context, packageName: String) {
        getSharedPreference(context).edit().putString(TRACKED_PACKEAGE_NAME, packageName).apply()
    }

    fun getTrackedPackageName(context: Context): String? = getSharedPreference(context).getString(TRACKED_PACKEAGE_NAME, null)

    fun saveStopped(context: Context, stopped: Boolean) {
        getSharedPreference(context).edit().putBoolean(STOPPED, stopped).apply()
    }

    fun isStopped(context: Context): Boolean = getSharedPreference(context).getBoolean(STOPPED, false)

    fun saveIsAppRunning(context: Context, state: Boolean) {
        getSharedPreference(context).edit().putBoolean(IS_APP_RUNNING, state).apply()
    }

    fun isAppRunning(context: Context): Boolean = getSharedPreference(context).getBoolean(IS_APP_RUNNING, false)
}