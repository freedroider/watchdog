package bm.poc.com.watchdogapp.ui.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.AppCompatSpinner
import android.widget.ArrayAdapter
import bm.poc.com.watchdogapp.R
import bm.poc.com.watchdogapp.core.Prefs
import bm.poc.com.watchdogapp.ui.activity.OnTrackedTimeListener

class SetTimeoutDialogFragment : DialogFragment() {
    companion object {
        fun newInstance() : SetTimeoutDialogFragment {
            val dialogFragment = SetTimeoutDialogFragment()
            return dialogFragment
        }
    }

    lateinit var listener: OnTrackedTimeListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(Prefs.getTime(activity) == 0L) {
            Prefs.saveTime(activity, 15L)
        }
        listener = activity as OnTrackedTimeListener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity).apply {
            setTitle("Tracking time")
            val view = activity.layoutInflater.inflate(R.layout.dialog_set_timeout, null)
            val items = arrayListOf("Seconds", "Minutes", "Hours")
            val adapter = ArrayAdapter<String>(context, R.layout.item_timeout_type, items)
            val spinnerSetTimeout = view.findViewById(R.id.spinnerSetTimeout) as AppCompatSpinner
            val etSetTimeout = view.findViewById(R.id.etSetTimeout) as AppCompatEditText
            spinnerSetTimeout.adapter = adapter
            val time = Prefs.getTime(context)
            etSetTimeout.setText(time.toString())
            setView(view)
            val selectedType = Prefs.getType(context)
            items.forEachIndexed { index, value ->
                val type = Prefs.toTimeType(value)
                if(type == selectedType) {
                    spinnerSetTimeout.setSelection(index)
                    return@forEachIndexed
                }
            }
            setPositiveButton(R.string.ok, {
                _, _ ->
                val selected = spinnerSetTimeout.selectedItemPosition
                val notFormatted = adapter.getItem(selected) as String
                Prefs.saveType(context, notFormatted)
                Prefs.saveTime(context, etSetTimeout.text.toString().toLong())
                listener.onOk()
                dismissAllowingStateLoss()
            })
        }
        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)
        return dialog
    }
}